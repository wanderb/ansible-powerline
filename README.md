# Ansible Powerline Setup

This role configures [Powerline](https://github.com/powerline/powerline) for
the remote user, using Python 3.x. It also configures the
[powerline-gitstatus](https://github.com/jaspernbrouwer/powerline-gitstatus)
segment.

It is meant as an example of how to configure Powerline, not as full role.

## Variables

Nothing to configure here.

## Documentation

For full documentation on Powerline, including setup, configuration, and fonts,
visit the [Powerline documentation site](https://powerline.readthedocs.io)

## Sample Playbook
```yaml
---
- name: Install and configure powerline for remote user
  hosts:
  - all
  become: false
  gather_facts: yes
  roles:
  - ansible-powerline
```
